# Exercice ligne de Commande Bash

## But: effectuer des manipulations de base sur le système de fichiers.

1. A partir de votre répertoire personnel, ou dans un dossier nommé Atelier, créez la structure suivante:

		.
		├── dossier1
		│   └── dossier3
		└── dossier2
			└── dossier4

2. Déplacez-vous dans le répertoire __dossier1__ avec un chemin adsolu et créez le fichier __fichier1__ dans ce répertoire.

3. Copiez __fichier1__ dans le répertoire __dossier3__ avec un chemin relatif.

4. Déplacez vous dans __dossier2__ en utilisant un chemin relatif, et copiez le fichier __fichier1__ de __dossier3__ sous le mon __fichier2__ là ou vous êtes.

5. Renommez et déplacez __fichier2__ en __fichier3__ dans le répertoire __dossier3__.

6. Supprimez __fichier1__ du repertoire __dossier3__.

7. Avec *rmdir* supprimez __dossier2__, puis __dossier1__ et tout son contenu, en une seule commande. Est-ce possible ? Pourquoi ? Comment faire ?

8. Créez un fichier appelé __-i__ avec une redirection *echo > -i* . Tentez de le supprimer.
