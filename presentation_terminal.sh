#!/bin/zsh
#
# ATTENTION, CE SCRIPT N'EST PAS TERMINÉ
#
# Ce script à pour but de fournir un support visuel pour la présentation des
# commandes de base.
# Pour l'utiliser il "suffit" de le lancer dans un shell Zsh et de commanter
# les commandes qui défilent au fur et à mesure, un appuye sur une touche
# passe à l'étape suivante. Une pause est volontairement inséré entre
# l'affichage d'une commande et l'affichage de sa sorti pour le confort
# visuel.
#
# TODO : Présenté ps et ses options les plus courantes
# TODO : Profiter de la présentation de ps pour abordé les "pipes" ou tunnels
#        (ps -aux | grep)
# TODO : Présenté kill
# TODO : Présenté pgrep et pkill ( vérifier qu'ils soit disponibles sous
#        Ubuntu et Mageia )
# TODO : Présenté les possibilitées étendu de la lignes de commande ( pipe,
# redirection, ;, &&, ||, joker * )
# TODO : Là, tout de suite, je n'ai plus d'idée

autoload colors && colors
alias ls='ls --color=auto'
alias grep='grep --color=auto'

local fake_prompt

function pause() {
	read -ks
}

function print_line() { # affiche une ligne de caractère = sur la largeur du terminal
	local -i count
	local line_char="="
	count=0
	while [[ $count -lt $COLUMNS ]]; do
		echo -n "$line_char"
		count+=1
	done
	echo
}

function print_function_name() { # Affiche $1 en vert en utilisant figlet, remplace les _ par des espaces
	echo -n ${fg[green]}
	figlet -f big -c -w ${COLUMNS} ${1:gs/_/ /}
	print_line
	echo -n ${reset_color}
}

function clear_screen() { # Efface l'écran et relance print_function_name $@
	clear
	print_function_name $@
}

function print_prompt() { # Affiche un "faux" prompt
	user="${fg[green]}${USER}${reset_color}"
	host="${fg[magenta]}${HOST}${reset_color}"
	working_directory="${fg[yellow]}${$(pwd):s/\/home\/fred/~/}${reset_color}"
	echo -n "${user}@${host} ${working_directory} $ "
}

function print_title() {
	echo "${fg[cyan]}${@}${reset_color}"
}

function print_and_launch_command() { # Affiche la commande passé en paramètre et la lance
	print_prompt

	echo -n $@
	pause
	echo

	eval $@
	if [[ -n "$(eval $@ 2> /dev/null)" ]];then
		pause
	fi
}

function controler_son_environnement_de_travail() { 
	clear_screen $0
	pause

	print_and_launch_command "pwd"

	clear_screen $0

	print_and_launch_command "ls"

	clear_screen $0

	print_and_launch_command "ls --color=auto"
	alias ls='ls --color=auto'

	clear_screen $0

	print_and_launch_command "ls -l"

	clear_screen $0

	print_and_launch_command "ls -la"

	clear_screen $0
}

function travailler_sur_des_fichiers() {
	clear_screen $0

	print_title "Créer et supprimer des fichiers et des répertoires"
	pause

	rm -rf presentation_terminal

	print_and_launch_command "mkdir presentation_terminal"
	print_and_launch_command "ls"
	clear_screen $0


	print_and_launch_command "cd presentation_terminal"
	print_and_launch_command "touch fichier1"
	print_and_launch_command "ls"
	clear_screen $0

	print_and_launch_command "mkdir dossier1"
	print_and_launch_command "ls"
	clear_screen $0

	print_and_launch_command "mv fichier1 dossier1"
	print_and_launch_command "ls -R"
	clear_screen $0

	print_and_launch_command "cd dossier1"

	print_and_launch_command "cp fichier1 fichier2"
	print_and_launch_command "ls"
	clear_screen $0

	print_and_launch_command "rm fichier1"
	print_and_launch_command "ls"
	clear_screen $0

	print_and_launch_command "cd .."
	print_and_launch_command "rm dossier1"
	print_and_launch_command "rm -r dossier1"
	clear_screen $0

	print_title "Trouver des fichiers"
	pause

	print_and_launch_command "find /etc -name 'passwd'"
	clear_screen $0
	
	print_and_launch_command "find /etc -name '*rules*' 2> /dev/null"
	clear_screen $0

	print_and_launch_command "locate passwd"
	clear_screen $0

	print_title "Afficher le contenue d'un fichier"
	pause

	print_and_launch "cat /etc/passwd"
	clear_screen $0

	print_and_launch_command "less /etc/passwd"
	clear_screen $0

	print_and_launch_command "more /etc/passwd"
	clear_screen $0

	print_title "Rechercher dans un fichier"
	pause

	print_and_launch_command "grep root /etc/passwd"
	clear_screen $0

	print_and_launch_command "grep '.sh' /etc/passwd"
	clear_screen $0

	print_and_launch_command "grep '\.sh' /etc/passwd"
	clear_screen $0

	print_and_launch_command "grep -F '.sh' /etc/passwd"
	clear_screen $0

}

travailler_sur_les_processus() {
	print_title "Lister les processus"
	pause

	print_and_launch_command "ps"
	print_and_launch_command "ps -aux"
	clear_screen $0
	
	print_title "Mais comment retrouver quelque chose la dedans?"
	pause

	print_and_launch_command "ps -aux | grep -i xorg"
	print_and_launch_command "pgrep Xorg"
	
	print_title "Il ne reste maintenant plus qu'à le tuer"
	print_prompt && echo -n "kill $(pgrep Xorg)"
	pause

	print_prompt && echo -n "pkill Xorg"
	pause
	
	print_title "Mais il y a plus simple : top et htop"
	print_and_launch_command "htop"
}

clear_screen "BONJOUR A TOUS"
pause

controler_son_environnement_de_travail
travailler_sur_des_fichiers
travailler_sur_les_processus

figlet -c "FIN"

# vim: foldmethod=indent
