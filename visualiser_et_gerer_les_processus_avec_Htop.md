## Visualiser et gérer les processus avec Htop

### But: Savoir gérer, rechercher, trier et tuer des processus.

+ Exécuter htop :
	- Il suffit de taper la commande ``htop`` dans un terminal.

+ L'interface :
![Interface](htop.png)
	- Dans la partie haute a gauche de l’interface on peut voir l’utilisation du processeur en pourcentage, l’utilisation de la mémoire en Mo par rapport à la totalité de mémoire disponible ainsi que la mémoire swap utilisée. En haut à droite, un petit résumé de l'activité du système : nombre total de processus, charge moyenne, temps écoulé depuis le dernier démarrage. Ensuite, un « tableau » listant les processus est présenté, où vous pouvez retrouver une multitude d’information comme l’utilisateur qu’il l’a exécuté, le pourcentage de mémoire et de charge processeur qu’il utilise ainsi que la commande qui sert à l’exécuter. Pour finir, dans la partie basse un menu explique les différentes actions qui sont disponibles.

+ Trier les processus selon un critère :
	- Si ont appuis sur la touche **F6**, ont peut choisir le critère de classement des processus (sort by). Ce critère peut être par l'utilisation de la mémoire (PERCENT_MEM), par utilisation du processeur (PERCENT_CPU), par état (STATE), par utilisateur (USER) ou par temps d'exécution (TIME).

+ Rechercher un processus :

	- Pour recherche un processus appuyez sur **F3** puis saisissez le nom ou une partie du nom de celui-ci. On peut remarquer que htop se positionne automatiquement sur le processus qui correspond à la recherche au fur et à mesure que l’on saisie.

+ Tuer un processus :
	- Positionnez vous sur le processus que vous désirez tuer, a l'aide du clavier ou de la souris, et appuyer sur la touche **F9**. Choisissez un la méthode de KILL que vous désirer, en général si le processus et complétement _gelée_, ont choisi la méthode ``SIGKILL``. Si vous désirer juste quitter le processus, utiliser la méthode ``SIGQUIT`` ou ``SIGTERM``.

+ Modifier la priorité d'un processus :
	- Pour modifier la priorité d’un processus positionnez vous dessus puis appuyez sur **F7** (Nice -) pour la diminuer et **F8** (Nice +) pour l’augmenter. On appelle cela le coefficient de _nice_ d'un processus.

+ L'aide :
	- On peut obtenir de l'aide sur htop en appuyant sur la touche **F1**, elle vous apprendra la signification des différents codes de couleurs utilisés ou celle des symboles de la colonne _statut_.

+ ``htop`` est conçu pour être totalement interactif. Il permet très facilement d'envoyer un signal à un processus, de changer sa priorité d'exécution, tout en contrôlant le résultat de ces actions sur la charge de l'ensemble du système. On peut effectuer des sélections multiples avec la touche [espace], et les actions s'appliqueront à l'ensemble de la sélection.
