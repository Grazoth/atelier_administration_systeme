# Solution exercice ligne de Commande Bash

## But : effectuer des manipulations de base sur le système de fichiers.

1. Le plus simple et le plus rapide est d'utiliser la commande ``__mkdir__`` avec l'option ``-p`` :
	
			$ mkdir -p dossier1/dossier3 dossier2/dossier4

	 L'argument *dossier1/dossier3* veut dire que l'ont va créer *dossier1* puis *dossier3* dans celui-ci. l'option ``-p`` ou ``--parents`` permet d'indiquer à mkdir de créer tout les répertoires parents nécessaires si ils n'existe pas.

2. Le chemin absolu part de la racine sans aucun chemin relatif. Tapez :
	
			$ cd /home/nom_de_l'utilisateur/dossier1

   Créez le fichier avec ``touch`` :

			$ touch fichier1

3. Le chemin relatif en fonction de l'emplacement actuel : __..__ remonte d'un niveau, et __.__ définit l'emplacement courant.
	
			$ pwd
			/home/nom_de_l'utilisateur/dossier1
   
    Copiez simplement le fichier dans __dossier3__, qui est la ou vous êtes :
	
			$ cp fichier1 dossier3

   ou bien :

			$ cp fichier1 ./dossier3

4. Déplacez-vous :

			$ cd ../dossier2

   Copiez le fichier :

			$ cp ../dossier1/dossier3 ./fichier2

5. La commande``mv`` déplace et renomme :

			$ mv fichier2 ../dossier1/dossier3/fichier3

6. La commande ``rm`` supprime le fichier :

			$ rm ../dossier1/dossier3/fichier1

7. Vous ne pouvez pas supprimer __dossier2__ directement avec ``rmdir`` car il contient __dossier4__, il n'est donc pas vide.

   Vous devez passer directement par la commande ``rm`` avec l'option ``-r`` :

			$ cd ..
			$ rm -r dossier2

8. Si vous tentez de supprimer ce fichier, ``rm`` retourne une erreur en indiquant qu'il manque un paramètre, car il interprète *-i* comme étant une option de la commande. 
 
   + Alors comment faire pour que *-i* ne soit pas reconnu comme une option ?

   Soit il faut indiquer à ``rm`` un chemin permettant d'isoler le tiret :

			$ rm ./-i

   Soit ont utilisent l'option ``--`` signifiant la fin des options et le début des arguments :

			$ rm -- -i

	
