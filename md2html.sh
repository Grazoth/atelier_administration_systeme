#!/bin/sh
# Converti en html le fichier markdown passé en argument et l'enregistre dans
# un fichier nom_du_fichier_d'origine.md.html.
# ce script nécessite l'installation du paquet markdown.
# 
# Le but de ce script est d'avoir un aperçu du rendu des fichiers markdown
# avant de pousser les modification dans le dépot.
# 
# Le fichier généré règle le jeu de caractère sur utf-8 et le titre de la page
# sur nom_du_fichier_d'origine.
# 
# Usage : ./script_name fichier_à_convertir

template="<!DOCTYPE html>
<html lang='fr'>
<head>
<meta charset='utf-8' />
<title>$(echo ${1} | cut -d . -f 1)</title>
</head>"

echo "${template}$(markdown ${1})" > ${1}.html

