# Organisation de l'atelier administration système du CERCLL : deuxième séance
_Note :_ Ceci n'est pas un programme définitif mais une base de travail pour
établir celui-ci.

## Programme envisagé de la séance
+ Présenter le partitionnement d'un volume de stockage
	- Les différents types de tables
		* MBR (Master Boot Record)
	  	     + La méthode partitionnement MBR date des années 80, à l'époque ou les PC travaillaient sur 16 bits, et a subi des modifications, que l'ont peut qualifier 		       de "bricolages" pour ensuite travailler sur 32 bits.
		       Cette méthode est seulement compatible avec PC équipés de puce (CMOS) BIOS. Les PC récents sont équipé de puce (CMOS) UEFI qui est incompatible avec le 			       MBR. Cette méthode peut seulement exploiter que des disques inférieurs à 2 To.
		* GPT (GUID Partition Table)
		     + La méthode de partitionnement GPT est compatible à la fois avec les puces (CMOS) UEFI et les puces BIOS. GPT et UEFI date de la fin des années 90 et ont 		       été initié par Intel. Son acceptation comme standard date de 2010.
                       GUID, signifiant Globaly Unique Identifier, est un identifiant de disque unique codé sur 128 bits, c'est l'équivalent de l'UUID, nom sous lequel il est   	                connu sous Linux et Unix. Un GUID ressemble à ceci: 8e86dffd-0d3c-4175-b2b9-515200cab17d
		       GPT est comptatible avec les disques de plus de 2 To.
	- Les différents types de systèmes de fichiers
		* ext2, ext3, ext4
		* reiserfs
		* xfs
		* zfs
		* btrfs ( prononcer butterfs )
		* vfat ( compatible avec tout )
		* ntfs ( Propriétaire, compatible MS-Windows, supporte les fichiers de
		  plus de 4Go.
	- Partitionnement manuel d'un disque. (voir Partitionement_disques.svg)
		* Partition 1: Partition Boot _/boot_ (en mode BIOS) formater en __ext4__ ou _/boot/efi_ (en mode UEFI) formater en __fat32__ (vfat) avec un label (flag) ESP ou ef00 en hexadecimal, sa taille peut faire entre 300 et 512 Mo.
		* Partition 2: Partition systeme _/_ (root, ne pas confondre avec __l'utilisateur root__) formater en __ext4__, taille entre 15 et 20 Gio.
		* Partition 3: Partition _Swap_, formater en __Swap Filesystem__, doit faire généralement le double de la mémoire vive (RAM), en général on ne met pas de Swap si ont a plus de 8Gio de RAM, ne pas mettre la Swap sur le disque SSD.
		* Partition 4: Partition home, _/home_, formater en __ext4__, c'est la partition qui contient le repertoire utilisateur, en général sa taille est ce qui reste de disponible sur le disque. L'interet de faire une partition séparé pour le dossier utilisateur, et que si l'ont doit ré-installer le systeme, ont peut ré-installer sans formater la partition _/home_, et donc ne pas éffacer les fichiers de l'utilisateur et les préférences des programmes. 
+ Présenter la gestion des services
	- Qu'est-ce qu'un service?
	- Présentation rapide des différents systèmes d'init
		* _SystemV_ : l'ancêtre ( prononcer system 5 )
		* _OpenRC_ : développer par l'équipe de netBSD
			+ Compatible scripts systemV
			+ Apporte des fonctionnalité supplémentaire par rapport à SystemV
		* _Upstart_ : développer pour Ubuntu, projet abandonné
		* _Les autres_ : faire la liste
		* _SystemD_ : "Nouveau" system qui a fait couler beaucoup d'entre,
			+ Disponible sur : 
				- Debian depuis la version 8
				- Ubuntu depuis la version 14.10 ( à confirmer )
				- Red Hat Entreprise Linux ( RHEL )
				- Fedora
				- OpenSUSE
				- Suse Linux Entreprise ( SLES )
				- Archlinux
				- _Jeu de piste :_ trouver les autres
			+ Incompatible avec tout les autres systèmes d'init
			+ Intègre un gestionnaire de journaux système
			+ Intègre un daemon type cron
			+ Intègre un bootloader
			+ __Intègre plein de truc à détailler__
			+ Devrait faire le café dans peu de temps
	- Expliquer les runlevels ( target sous SystemD )
	- Montrer comment démarrer, arréter et obtenir le statut d'un service
	- Montrer comment lancer un service au démarrage
+ Présenter l'arborescence 
	- Une arborescence unique pour tout les systèmes de fichiers contrairement
	  à windows
	- Présentation des répertoire les plus significatif pour les débutants
		* /boot
		* /bin /usr/bin 
		* /etc
		* /proc /sys
		* /var
		* /media ou /run/media selon les distributions
		* /home et /root
		* /dev
	- Principe du montage
	- …?
+ Mettre un peu de couleurs dans le terminal
	- Donner et expliquer rapidement ( 5 minutes max ) la config qui permet
	  d'avoir de joli couleur dans _man_ et dans le prompt
		* Dans _man_ parce que la coloration syntaxique c'est la vie,
		* Dans le prompt parce que retrouver le prompt précédent après une
		  sorti dont on ne sait pas si elle fait 10 ou 10K lignes n'est pas
		  toujours évident
+ Présenter Zsh qui est à mon avis ( Fred ) un gros plus pour les débutants
	* Parce qu'il propose un auto complètement de ouf
		- Gère les erreurs de casse ( _tél_ sera complété par _Téléchargement_ si
		  aucun fichier ne commence par _tél_ )
		- Complète quelque soit la position de la frappe dans le nom de
		  fichier ( _txt_ sera complété par _salut.txt_, _os_ sera complété
		  par _30_os-prober_, _2_ par _fichier_2_ … )
		- Complète sur les noms fichiers, les options des commandes, les noms
		  d'hotes ( Compléter ce qui aurait pu être oublié )
		- Occasionne parfois des erreurs énervantes en cas de faute de frappe
		  ( _se_ sera complété par *dessins_animés* alors que l'on parle du
		  dossier _séries_ )
		- …
	* Parce que ZLE ( l'invite de commande ) permet de mettre facilement de la
	  couleur [Voir la page Github du projet _zsh syntax
	  highlighting_](https://github.com/zsh-users/zsh-syntax-highlighting)
	* Parce qu'il propose de corriger les commandes en cas de faute de frappe
		- _mkdr_ proposera _mkdir_
		- _sdo_ proposera  sudo_
		- _locte_ ploposera _locate_
		- …
		- Les corrections proposé ne sont pas toujours pertinante
+ Présenter le principe de la coloration syntaxique pour l'édition facile des
  fichiers de config

## Commandes de bases
Les commandes à présenter obligatoirement, l'important n'est pas de les retenir, mais d'en avoir entendu parler pour les retrouver en cas de besoin

+ man

+ ls
+ cd
+ pwd

+ cp
+ mv
+ mkdir
+ rm/rmdir

+ cat
+ less

+ find
+ locate
+ grep

+ sudo

+ ps
+ kill
+ pgrep et pkill

+ dd ( juste pour prévenir des catastrophe qu'elle peut engendrer afin qu'ils
  soit prudent si par malheurs ils trouvent sur le web un post qui propose de
  l'utiliser. __dd = disk destructor__ )

## Ligne de commande étendu
+ Enchainements des commandes
	- ;
	- |
	- &&
	- ||

+ Redirections
	- >
	- 2>
	- (2>&1) ?

+ Autres
	- *
	- & ?
	- {,} ?

## Logiciels complémentaires
+ htop
+ ncdu
+ pydf
+ free

